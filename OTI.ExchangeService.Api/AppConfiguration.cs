﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTI.ExchangeService.Api
{
    public class AppConfiguration
    {
        public string ExchangeServiceUrl { get; set; }
    }
}
