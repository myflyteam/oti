using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Microsoft.OpenApi.Models;
using OTI.CSVExport;
using OTI.Exchange;
using OTI.Exchange.TCMB;
using OTI.Shared.Mvc.Extensions;
using OTI.Shared.Serializations;
using OTI.XmlExport;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTI.ExchangeService.Api
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllers().AddXmlSerializerFormatters();

            //  services.AddControllers();
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "OTI.ExchangeService.Api", Version = "v1" });
            });



            services.Configure<AppConfiguration>(options => Configuration.GetSection("AppConfiguration").Bind(options));

            services.AddScoped<IExchangeService>((s) =>
            {
                var config = s.GetRequiredService<IOptions<AppConfiguration>>();
                return new OTI.Exchange.TCMB.TCMBExchangeService(config.Value.ExchangeServiceUrl);
            });

            services.AddScoped<IExportServiceFactory, ExportServiceFactory>();
            services.AddScoped<IExportService, XmlExportService>();
            services.AddScoped<IExportService, CSVExportService>();
            services.AddScoped<IContentExport, ContentExport>();
            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "OTI.ExchangeService.Api v1"));
            }

            app.UseHttpsRedirection();

            app.UseRouting();

            app.UseAuthorization();

            app.UseApiExceptionHandling();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
