﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OTI.Exchange;
using OTI.Exchange.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTI.ExchangeService.Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ExportController : ControllerBase
    {
        private readonly IContentExport contentExport;

        public ExportController(IContentExport contentExport)
        {
            this.contentExport = contentExport ?? throw new ArgumentNullException(nameof(contentExport));
        }


        [HttpGet]
        public async Task<ActionResult<ExportContentResponse>> Get([FromQuery] ExportContentRequest request)
        {
            var response = await contentExport.AsAsync(request);
            return Ok(response);
        }
    }
}
