﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OTI.Exchange;
using OTI.Exchange.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace OTI.ExchangeService.Api.Controllers
{

    [Route("api/[controller]")]
    [ApiController]
    public class CurrencyRateController : ControllerBase
    {
        private readonly IExchangeService exchangeService;

        public CurrencyRateController(IExchangeService exchangeService)
        {
            this.exchangeService = exchangeService ?? throw new ArgumentNullException(nameof(exchangeService));
        }

        [HttpPost]
        public async Task<ActionResult<GetRatesResponse>> Post([FromBody] GetRatesRequest request)
        {
            var response = await exchangeService.GetRatesAsync(request);
            return Ok(response);
        }

        [HttpGet]
        public async Task<ActionResult<GetRateResponse>> Get([FromQuery] GetRateRequest request)
        {
            var response = await exchangeService.GetRateAsync(request);
            return Ok(response);
        }
    }
}
