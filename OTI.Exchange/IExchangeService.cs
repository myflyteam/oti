﻿using OTI.Exchange.DataTransferObjects;
using System.Threading.Tasks;

namespace OTI.Exchange
{
    public interface IExchangeService
    {
        Task<GetRateResponse> GetRateAsync(GetRateRequest request);
        Task<GetRatesResponse> GetRatesAsync(GetRatesRequest request);
    }
}
