﻿using OTI.Exchange.DataTransferObjects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Exchange
{
    public interface IContentExport
    {
        Task<ExportContentResponse> AsAsync(ExportContentRequest request);
    }
}
