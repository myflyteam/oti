﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace OTI.Exchange.Lib.Models
{
    public class CurrencyRate
    {
        public List<Currency> Currencies { get; set; }
    }
    public class Currency
    {
        public decimal Unit { get; set; }
        public string Isim { get; set; }
        public string CurrencyName { get; set; }
        public decimal? ForexBuying { get; set; }
        public decimal? ForexSelling { get; set; }
        public decimal? BanknoteBuying { get; set; }
        public decimal? BanknoteSelling { get; set; }
        public string CurrencyCode { get; set; }
    }
}
