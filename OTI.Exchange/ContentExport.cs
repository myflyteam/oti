﻿using OTI.Exchange.DataTransferObjects;
using OTI.Exchange.Lib.Models;
using OTI.Shared.Serializations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Exchange
{
    public class ContentExport : IContentExport
    {
        private readonly IExportServiceFactory exportServiceFactory;
        private readonly IExchangeService exchangeService;

        public ContentExport(
            IExportServiceFactory exportServiceFactory,
            IExchangeService exchangeService)
        {
            this.exportServiceFactory = exportServiceFactory ?? throw new ArgumentNullException(nameof(exportServiceFactory));
            this.exchangeService = exchangeService ?? throw new ArgumentNullException(nameof(exchangeService));
        }


        public async Task<ExportContentResponse> AsAsync(ExportContentRequest request)
        {
            var service = exportServiceFactory.GetInstance(request.MeediaTypeCode);
            var rates = await exchangeService.GetRatesAsync(request);
            return new ExportContentResponse
            {
                Content = service.Init(rates.Currencies),
                MeediaTypeCode = request.MeediaTypeCode
            };
        }
    }
}
