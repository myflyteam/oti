﻿using OTI.Exchange.Lib.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Exchange.DataTransferObjects
{
    public class GetRateRequest
    {
        [StringLength(3, MinimumLength = 3, ErrorMessage = "Currency Code length can't be more than 3.")]
        public string CurrencyCode { get; set; }
    }

    public class GetRateResponse
    {
        public Currency Currency { get; set; }
    }
}
