﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Exchange.DataTransferObjects
{
    public class ExportContentRequest : GetRatesRequest
    {
        public string MeediaTypeCode { get; set; }
    }
    public class ExportContentResponse
    {
        public string MeediaTypeCode { get; set; }
        public string Content { get; set; }
    }
}
