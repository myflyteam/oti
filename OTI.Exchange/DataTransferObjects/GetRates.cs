﻿using OTI.Exchange.Lib.Models;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Exchange.DataTransferObjects
{
    public class GetRatesRequest
    {
        public GetRatesRequest()
        {
            SortItems = new List<SortItem>();
        }
        public List<SortItem> SortItems { get; set; }
    }


    public class SortItem
    {
        public int OrderNo { get; set; }

        [StringLength(50, MinimumLength = 3, ErrorMessage = "PropertyName length can be between 3-50.")]
        public string PropertyName { get; set; }

        public bool IsAscending { get; set; }
    }

    public class GetRatesResponse
    {
        public GetRatesResponse()
        {
            Currencies = new List<Currency>();
        }
        public List<Currency> Currencies { get; set; }
    }
}
