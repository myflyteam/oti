﻿using OTI.CSVExport;
using OTI.Exchange;
using OTI.Exchange.DataTransferObjects;
using OTI.Exchange.Lib.Models;
using OTI.Exchange.TCMB;
using OTI.Exchange.TCMB.Exceptions;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace OTI.Sandbox
{
    class Program
    {
        static async Task Main(string[] args)
        {
            IExchangeService service = new TCMBExchangeService("http://www.tcmb.gov.tr/kurlar/today.xml");

            try
            {

                var rates = await service.GetRatesAsync(new GetRatesRequest()
                {
                    SortItems = new List<SortItem>()
                {
                    new SortItem() { OrderNo = 1, PropertyName = "ForexSelling", IsAscending = false },
                    new SortItem() { OrderNo = 2, PropertyName = "ForexBuying", IsAscending = true }       }
                });

                var rate = await service.GetRateAsync(new GetRateRequest()
                {
                    CurrencyCode = "USD"
                });

                CSVExportService csv = new();

                var result = csv.Init(rates.Currencies);
            }
            catch (TCMBException exception)
            {
                Console.WriteLine(exception.Message);
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }







            Console.ReadLine();
        }
    }
}
