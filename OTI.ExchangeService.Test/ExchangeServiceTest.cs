using NUnit.Framework;
using OTI.Exchange.DataTransferObjects;
using OTI.Exchange.TCMB;
using OTI.Exchange.TCMB.Exceptions;

namespace OTI.ExchangeService.Test
{
    public class ExchangeServiceTest
    {
        [SetUp]
        public void Setup()
        {
        }

        [Test]
        public void GetRates_ThrowException_WhenUrlIncorrect()
        {
            string fakeUrl = "";
            Assert.Throws(typeof(TCMBException), () => new TCMBExchangeService(fakeUrl).GetRatesAsync(null));
        }

        [Test]
        public void GetRates_ThrowException_WhenPropertyNameInvalid()
        {
            string url = "http://www.tcmb.gov.tr/kurlar/today.xml";
            string fakeProperty = "FakeProperty";
            var request = new GetRatesRequest()
            {
                SortItems = new System.Collections.Generic.List<SortItem>()
                {
                    new SortItem(){IsAscending=true, OrderNo=0,PropertyName=fakeProperty}
                }
            };
            Assert.Throws(typeof(PropertyNameNotFoundEx), () => new TCMBExchangeService(url).GetRatesAsync(request));
        }
    }
}