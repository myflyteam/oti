﻿using OTI.Shared.Reflections;
using OTI.Shared.Serializations;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Xml;
using System.Xml.Serialization;

namespace OTI.XmlExport
{
    public class XmlExportService : IExportService
    {
        public string MediaTypeCode => "XML";

        public string Init<T>(List<T> collection) where T : class
        {
            var serializer = new XmlSerializer(typeof(List<T>));
            var sw = new StringWriter();
            serializer.Serialize(sw, collection);

            return sw.ToString();
        }
    }
}
