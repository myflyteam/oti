﻿using OTI.Exchange.DataTransferObjects;
using OTI.Exchange.Lib.Models;
using OTI.Exchange.TCMB.Exceptions;
using OTI.Shared.Reflections;
using System.Linq;
using System.Threading.Tasks;

namespace OTI.Exchange.TCMB
{
    public class TCMBExchangeService : IExchangeService
    {
        private readonly string todayXmlUrl;


        public TCMBExchangeService(string todayXmlUrl)
        {
            this.todayXmlUrl = todayXmlUrl ?? throw new System.ArgumentNullException(nameof(todayXmlUrl));
        }


        public async Task<GetRateResponse> GetRateAsync(GetRateRequest request)
        {
            XmlService service = new(todayXmlUrl);
            await service.LoadXmlAsync();

            return new GetRateResponse()
            {
                Currency = service.GetCurrency(request.CurrencyCode)
            };
        }


        public async Task<GetRatesResponse> GetRatesAsync(GetRatesRequest request)
        {
            XmlService service = new(todayXmlUrl);
            await service.LoadXmlAsync();
            var items = service.GetCurrencies().OrderBy(x => 1);

            foreach (var sort in request.SortItems.OrderBy(x => x.OrderNo))
            {
                if (!PropertyAccessor<Currency>.IsExist(sort.PropertyName))
                {
                    var properties = string.Empty;
                    try
                    {
                        properties = string.Join(',', PropertyAccessor<Currency>.GetNames());
                    }
                    finally
                    {
                        throw new PropertyNameNotFoundEx($"The specified proparty name {sort.PropertyName} not found! Availables are {properties}");
                    }

                }
                if (sort.IsAscending)
                {
                    items = items.OrderBy(x => PropertyAccessor<Currency>.Get(x, sort.PropertyName));
                }
                else
                {
                    items = items.OrderByDescending(x => PropertyAccessor<Currency>.Get(x, sort.PropertyName));
                }
            }

            return new GetRatesResponse
            {
                Currencies = items.ToList()
            };
        }

    }
}
