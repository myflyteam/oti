﻿using OTI.Exchange.Lib.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using OTI.Shared.Extensions;
using System.Xml.Linq;
using OTI.Exchange.TCMB.Exceptions;

namespace OTI.Exchange.TCMB
{
    internal class XmlService
    {
        private readonly string todayXmlUrl;
        private const string ROOT_ELEMENT = "Tarih_Date";
        private XmlDocument xmlDoc = null;

        public XmlService(string todayXmlUrl)
        {
            this.todayXmlUrl = todayXmlUrl;
        }
        public Currency GetCurrency(string currencyCode)
        {
            var note = xmlDoc.SelectSingleNode(FormatCurrencyPath(currencyCode));
            return ToCurrency(note);
        }

        public IEnumerable<Currency> GetCurrencies()
        {
            foreach (XmlNode node in xmlDoc.SelectNodes(GetCurrenciesPath()))
            {
                yield return ToCurrency(node);
            }
        }


        private Currency ToCurrency(XmlNode element)
        {
            if (element is null)
            {
                throw new ArgumentNullException(nameof(element));
            }

            return new Currency()
            {
                Unit = element["Unit"].ToDecimal(),
                BanknoteBuying = element["BanknoteBuying"].ToNullOrDecimal(),
                BanknoteSelling = element["BanknoteSelling"].ToNullOrDecimal(),
                CurrencyCode = element.Attributes["CurrencyCode"].ToStringOrNull(),
                CurrencyName = element["CurrencyName"].ToStringOrNull(),
                ForexBuying = element["ForexBuying"].ToNullOrDecimal(),
                ForexSelling = element["ForexSelling"].ToNullOrDecimal(),
                Isim = element["Isim"].ToStringOrNull()
            };
        }

        public Task LoadXmlAsync()
        {
            return Task.Run(() =>
             {
                 xmlDoc = GetXmlDocument();
             });
        }

        private XmlDocument GetXmlDocument()
        {
            try
            {
                var document = new XmlDocument();
                document.Load(todayXmlUrl);
                return document;
            }
            catch (Exception exception)
            {
                throw new XmlLoadException("Exception occured while xml is loading!", exception);
            }
        }

        private string FormatCurrencyPath(string currencyCode)
        {
            return $"{ROOT_ELEMENT}/Currency[@CurrencyCode='{currencyCode.ToUpper()}']";
        }

        private string GetCurrenciesPath()
        {
            return $"{ROOT_ELEMENT}/Currency";
        }
    }
}
