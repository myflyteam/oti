﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Exchange.TCMB.Exceptions
{
    public class PropertyNameNotFoundEx : TCMBException
    {
        public PropertyNameNotFoundEx(string message) : base(message)
        {

        }
        public PropertyNameNotFoundEx(string message, Exception exception) : base(message, exception)
        {

        }
    }
}
