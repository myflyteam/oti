﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Exchange.TCMB.Exceptions
{
    public class XmlLoadException : TCMBException
    {
        public XmlLoadException(string message) : base(message)
        {

        }
        public XmlLoadException(string message, Exception exception) : base(message, exception)
        {

        }
    }
}
