﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Shared.Reflections
{
    internal static class PropertyProvider<T> where T : class
    {
        private static readonly ConcurrentBag<ObjectDescription> items = new();
        private static readonly object locker = new();

        public static ObjectDescription Get()
        {
            var description = items.FirstOrDefault(x => x.ObjectType == typeof(T));

            if (description == null)
            {
                lock (locker)
                {
                    description = items.FirstOrDefault(x => x.ObjectType == typeof(T));
                    if (description == null)
                    {
                        description = DescriptionCreator.Create<T>();
                        items.Add(description);
                    }
                }
            }
            return description;
        }


    }
}
