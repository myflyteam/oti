﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Shared.Reflections
{
    public class PropertyAccessor<T> where T : class
    {
        public static object Get(T instance, string propertyName)
        {
            if (instance == null)
            {
                throw new ArgumentException($"'{nameof(propertyName)}' cannot be null.", nameof(instance));
            }

            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentException($"'{nameof(propertyName)}' cannot be null or empty.", nameof(propertyName));
            }

            var getter = PropertyProvider<T>.Get().PropertyMembers.First(x => x.Name == propertyName).Getter;
            var value = getter.DynamicInvoke(instance);
            return value;

        }

        public static bool IsExist(string propertyName)
        {
            if (string.IsNullOrEmpty(propertyName))
            {
                throw new ArgumentException($"'{nameof(propertyName)}' cannot be null or empty.", nameof(propertyName));
            }

            return PropertyProvider<T>.Get().PropertyMembers.Any(x => x.Name == propertyName);
        }
        public static IEnumerable<string> GetNames()
        {
            return PropertyProvider<T>.Get().PropertyMembers.Select(x => x.Name);
        }
    }
}
