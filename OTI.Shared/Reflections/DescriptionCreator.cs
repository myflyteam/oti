﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Shared.Reflections
{
    public static class DescriptionCreator
    {

        public static ObjectDescription Create<T>() where T : class
        {

            var description = new ObjectDescription(typeof(T));

            foreach (var property in typeof(T).GetProperties())
            {
                Delegate getter = CreateGetter(property);
                Delegate setter = CreateSetter(property);
                description.PropertyMembers.Add(new PropertyMembers()
                {
                    Getter = CreateGetter(property),
                    Setter = CreateGetter(property),
                    Name = property.Name,
                    Info = property
                });
            }
            return description;
        }

        public static Delegate CreateGetter(PropertyInfo property)
        {
            var objParm = Expression.Parameter(property.DeclaringType, "o");
            Type delegateType = typeof(Func<,>).MakeGenericType(property.DeclaringType, property.PropertyType);
            var lambda = Expression.Lambda(delegateType, Expression.Property(objParm, property.Name), objParm);
            return lambda.Compile();
        }

        public static Delegate CreateSetter(PropertyInfo property)
        {
            var objParm = Expression.Parameter(property.DeclaringType, "o");
            var valueParm = Expression.Parameter(property.PropertyType, "value");
            Type delegateType = typeof(Action<,>).MakeGenericType(property.DeclaringType, property.PropertyType);
            var lambda = Expression.Lambda(delegateType, Expression.Assign(Expression.Property(objParm, property.Name), valueParm), objParm, valueParm);
            return lambda.Compile();
        }
    }
}
