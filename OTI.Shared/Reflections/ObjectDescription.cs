﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Shared.Reflections
{
    public class ObjectDescription
    {
        public ObjectDescription(Type objectType)
        {
            ObjectType = objectType;
            PropertyMembers = new List<PropertyMembers>();
        }
        public Type ObjectType { get; set; }

        public List<PropertyMembers> PropertyMembers { get; set; }
    }
}
