﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Shared.Reflections
{
    public class PropertyMembers
    {
        public string Name { get; set; }
        public Delegate Getter { get; set; }
        public Delegate Setter { get; set; }
        public PropertyInfo Info { get; set; }
    }
}
