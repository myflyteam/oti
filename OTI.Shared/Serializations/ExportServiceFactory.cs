﻿using OTI.Shared.Serializations.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Shared.Serializations
{
    public class ExportServiceFactory : IExportServiceFactory
    {
        private readonly IEnumerable<IExportService> exportServices;

        public ExportServiceFactory(IEnumerable<IExportService> exportServices)
        {
            this.exportServices = exportServices;
        }
        public IExportService GetInstance(string typeCode)
        {
            var service = exportServices.FirstOrDefault(x => x.MediaTypeCode == typeCode.ToUpper());

            if (service == null)
                throw new UnSupportedMediaTypeException($" there is no service with {typeCode} ");

            return service;
        }
    }
}
