﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Shared.Serializations.Exceptions
{
    public class UnSupportedMediaTypeException : Exception
    {
        public UnSupportedMediaTypeException(string message) : base(message)
        {

        }
    }
}
