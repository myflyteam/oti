﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Shared.Serializations
{
    public interface IExportServiceFactory
    {
        IExportService GetInstance(string typeCode);
    }
}
