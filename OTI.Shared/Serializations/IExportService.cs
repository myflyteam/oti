﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace OTI.Shared.Serializations
{
    public interface IExportService
    {
        string MediaTypeCode { get; }

        string Init<T>(List<T> collection) where T : class;
    }
}
