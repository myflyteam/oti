﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace OTI.Shared.Extensions
{
    public static class XmlElementExtension
    {
        public static decimal? ToNullOrDecimal(this XmlAttribute xmlAttribute)
        {
            decimal? result = null;
            if (xmlAttribute == null)
            {
                return null;
            }
            else
            {
                decimal value = 0;
                if (decimal.TryParse(xmlAttribute.Value, out value))
                {
                    result = value;
                }

            }
            return result;
        }

        public static decimal ToDecimal(this XmlAttribute xmlAttribute)
        {
            decimal result = 0;
            if (xmlAttribute != null)
            {

                decimal value = 0;
                if (decimal.TryParse(xmlAttribute.Value, out value))
                {
                    result = value;
                }
            }
            return result;
        }

        public static string ToStringOrNull(this XmlAttribute xmlAttribute)
        {
            if (xmlAttribute == null)
            {
                return null;
            }
            else
            {
                return xmlAttribute.Value.ToString();
            }
        }



        public static decimal? ToNullOrDecimal(this XmlElement element)
        {
            decimal? result = null;
            if (element == null)
            {
                return null;
            }
            else
            {
                decimal value = 0;
                if (decimal.TryParse(element.InnerText, out value))
                {
                    result = value;
                }

            }
            return result;
        }

        public static decimal ToDecimal(this XmlElement element)
        {
            decimal result = 0;
            if (element != null)
            {

                decimal value = 0;
                if (decimal.TryParse(element.InnerText, out value))
                {
                    result = value;
                }
            }
            return result;
        }

        public static string ToStringOrNull(this XmlElement element)
        {
            if (element == null)
            {
                return null;
            }
            else
            {
                return element.InnerText.ToString();
            }
        }
    }
}
