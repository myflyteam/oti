﻿using OTI.Shared.Reflections;
using OTI.Shared.Serializations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace OTI.CSVExport
{
    public class CSVExportService : IExportService
    {
        public string MediaTypeCode { get => "CSV"; }


        public string Init<T>(List<T> collection) where T : class
        {
            var properties = PropertyAccessor<T>.GetNames();

            StringBuilder builder = new();
            string header = string.Join(',', properties);
            builder.AppendLine(header);

            foreach (var item in collection)
            {
                builder.AppendLine(string.Join(',', properties.Select(x => PropertyAccessor<T>.Get(item, x))));
            }
            return builder.ToString();
        }
    }
}
